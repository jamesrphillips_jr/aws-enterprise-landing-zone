# AppDev Shared Services Account

These stacks should be deployed to nonprod and prod accounts

## Resources

* VPC /20
* 4 Availability Zones

* 4 isolated subnets /24
* 4 private subnets(private_b) /23
* 4 transit gateway connect subnets (private_c) /24

* Default security group with no access
