# Egress Account(s)

These stacks should be deployed to nonprod and prod accounts

## Resources

* VPC /20
* 2 Availability Zones
* 1 public subnet (public_elb) /24
* 1 Internet Gateway
* 1 NAT Gateway
* 1 private subnet (firewall) /24
* 1 private subnet (private_elb) /24
* 1 private subnet (connectivity) /24

* Default security group with no access
