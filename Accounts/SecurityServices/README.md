# Security Services Account(s)

These stacks should be deployed to nonprod and prod accounts

## Resources

* VPC /20
* 2 Availability Zones

* 1 private subnet (private_a) /24
* 1 private subnet (private_b) /24
* 1 private subnet (transit_a) /24
* 1 private subnet (transit_b) /24

* Default security group with no access
