# Shared Services Account(s)

These stacks should be deployed to nonprod and prod accounts

## Resources

* VPC /20
* 2 Availability Zones (address space allocated for 4 AZ use in us-east-1 region)

* 2 isolated subnets /24
* 2 private subnets(private_b) /23
* 2 transit gateway connect subnets (private_c) /24

* Default security group with no access
